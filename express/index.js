const express = require("express");
const config = require("config");
const cors = require("cors");
const bodyParser = require("body-parser");
const reactionRoutes = require("./routes/reaction-routes");

const PORT = config.get("port") ?? 5000;

const app = express();

app.use(express.json());
app.use(cors());
app.use(bodyParser.json());

app.use("/api", reactionRoutes.routes);

async function start() {
  try {
    app.listen(PORT, () => {
      console.log("server started...");
      console.log("Port", PORT);
    });
  } catch (e) {
    console.log("Server error:", e);
    process.exit(1);
  }
}

start();
