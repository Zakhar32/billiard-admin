const { delay, getRandomInt, getMax } = require("./utils");
const fetch = require("node-fetch");
const firebase = require("../db");
const firestore = firebase.firestore();

class Liker {
  constructor() {
    this.isStop = false;
    this.current = 0;
  }

  stop() {
    this.isStop = true;
  }

  start(id, channelId, messageId, reactions, tokens) {
    const max = getMax(reactions, tokens.length);

    const loop = async () => {
      if (this.isStop) {
        return;
      }

      const randomTime = getRandomInt(Math.abs(2), Math.abs(4));

      await setTimeout(async () => {
        for (const reaction of reactions) {
          if (this.current < tokens.length * (reaction.part / 100)) {
            await delay(500);

            const resp = await fetch(
              `https://discord.com/api/v9/channels/${channelId}/messages/${messageId}/reactions/${reaction.value}/%40me`,
              {
                method: "PUT",
                headers: {
                  Authorization: tokens[this.current],
                },
                body: JSON.stringify({}),
              }
            );

            console.log(resp.ok, this.current);
          }
        }

        this.current += 1;

        if (this.current < max) {
          loop();
        } else if (this.current === Math.ceil(max)) {
          const currReaction = await firestore.collection("reactions").doc(id);
          const newData = await currReaction.update({
            isActive: false,
            isFinish: true,
          });
        }
      }, randomTime * 1000);
    };

    loop();
  }
}

module.exports = Liker;
