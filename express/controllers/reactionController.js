const firebase = require("../db");
const Reaction = require("../models/Reaction");
const firestore = firebase.firestore();

const Liker = require("../liker/Liker");

const reactionsMap = new Map();

const addReaction = async (req, res, next) => {
  try {
    const data = req.body;
    const newData = await firestore.collection("reactions").add(data);

    const liker = new Liker();
    liker.start(
      newData.id,
      data.channelId,
      data.messageId,
      data.reactions,
      data.tokens
    );
    reactionsMap.set(newData.id, liker);

    res.send({ message: "Reaction saved successfully" });
  } catch (error) {
    res.status(400).send({ message: error.message });
  }
};

const getAllReactions = async (req, res, next) => {
  try {
    const reactions = await firestore.collection("reactions");
    const data = await reactions.get();
    const reactionsArray = [];

    data.forEach((doc) => {
      const reaction = new Reaction(
        doc.id,
        doc.data().name,
        doc.data().channelId,
        doc.data().messageId,
        doc.data().reactions,
        doc.data().isActive,
        doc.data().isFinish,
        doc.data().tokens
      );
      reactionsArray.push(reaction);
    });
    res.send(reactionsArray);
  } catch (error) {
    res.status(400).send({ message: error.message });
  }
};

const updateReaction = async (req, res, next) => {
  try {
    const id = req.params.id;
    const data = req.body;
    const reaction = await firestore.collection("reactions").doc(id);
    const newData = await reaction.update(data);

    if (data.isFinish) {
      reactionsMap.get(id)?.stop();
    }

    res.send({ message: "Reaction record updated successfully" });
  } catch (error) {
    res.status(400).send({ message: error.message });
  }
};

const getReactionProgress = async (req, res, next) => {
  try {
    const id = req.params.id;
    const reaction = await firestore.collection("reactions").doc(id);

    const progress = reactionsMap.get(reaction?.id)?.current;

    if (!progress) {
      res.status(400).send({ message: "Id not found" });
    } else {
      res.send({ progress });
    }
  } catch (error) {
    res.status(400).send({ message: error.message });
  }
};

module.exports = {
  addReaction,
  getAllReactions,
  updateReaction,
  getReactionProgress,
};
